class AccountActivationsController < ApplicationController
  def edit
    user = User.find_by(email: params[:email])
    if user && !user.activated? && user.authenticated?(:activation, params[:id])
      user.activate
      user.update_attribute(:activated,    true)
      user.update_attribute(:activated_at, Time.zone.now)
      log_in user
      flash[:success] = "계정이 활성화 되었습니다!!"
      redirect_to user
    else
      flash[:danger] = "유효하지 않은 활성 링크입니다!"
      redirect_to root_url
    end
  end
end
